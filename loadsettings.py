import json
import os
import sys

from display_types import *
from particles import ParticleSystem, Point

_path = os.path.abspath('')

try:
    file_path = sys.argv[1]
    if not os.path.isabs(file_path):
        file_path = _path + '/' + file_path
    settings = json.loads(open(file_path, 'r').read())
except IndexError:
    print("Usage: python3 " + sys.argv[0] + " /path/to/settings.json")
    exit(-1)
except FileNotFoundError:
    print("No such file : " + file_path)
    exit(-1)

try:
    default_settings = json.loads(open(_path+"/default.json", 'r').read())
    is_redirect_default = default_settings["redirect"]
    _WINDOW = "window"
    is_fullscreen_default = bool(default_settings[_WINDOW]["fullscreen"])
    W_default = int(default_settings[_WINDOW]["width"])
    H_default = int(default_settings[_WINDOW]["height"])
    is_clearscreen_default = bool(default_settings[_WINDOW]["clear_screen"])
    image_path_default = default_settings[_WINDOW]["image_save_directory"]
    bg_color_default = default_settings[_WINDOW]["bg_color"]
    _WORLD = "world"
    k_default = int(default_settings[_WORLD]["k"])
    dt_default = float(default_settings[_WORLD]["dt"])
    _ORIGIN = "origin"
    origin_x_default = int(default_settings[_ORIGIN]["x"])
    origin_y_default = int(default_settings[_ORIGIN]["y"])
    is_followmouse_default = bool(default_settings[_ORIGIN]["follow_mouse"])
    origin_q_default = int(default_settings[_ORIGIN]["q"])
    is_visible_default = bool(default_settings[_ORIGIN]["visible"])
    origin_color_default = default_settings[_ORIGIN]["color"] if is_visible_default else None
except FileNotFoundError:
    print("Failed to load default settings file!")
except KeyError:
    print("Default settings are missing options!")


try:
    is_redirect = settings["redirect"]
except KeyError:
    is_redirect = is_redirect_default
finally:
    if is_redirect not in ("none", None, 0, ""):
        settings = json.loads(open(settings["redirect"], 'r').read())

_WINDOW = "window"
try:is_fullscreen = bool(settings[_WINDOW]["fullscreen"])
except KeyError:is_fullscreen = is_fullscreen_default
try: W = int(settings[_WINDOW]["width"])
except KeyError:  W = W_default
try: H = int(settings[_WINDOW]["height"])
except KeyError: H = H_default
try: is_clearscreen = bool(settings[_WINDOW]["clear_screen"])
except KeyError: is_clearscreen = is_clearscreen_default
try: image_path = settings[_WINDOW]["image_save_directory"]
except KeyError: image_path = image_path_default
try: bg_color = settings[_WINDOW]["bg_color"]
except KeyError: bg_color = bg_color_default
_WORLD = "world"
try: k = int(settings[_WORLD]["k"])
except KeyError: k = k_default
try: dt = float(settings[_WORLD]["dt"])
except KeyError: dt = dt_default

_PARTICLES = "particles"
particle_systems = {}
for system_name in settings[_PARTICLES].keys():
    try:
        particle_display_type = settings[_PARTICLES][system_name]["type"]
        if particle_display_type in TYPE_PIXEL:
            display_type = TYPE_PIXEL
        elif particle_display_type in TYPE_CIRCLE:
            display_type = TYPE_CIRCLE
        is_respawn = bool(settings[_PARTICLES][system_name]["respawn"])
        interact_with = settings[_PARTICLES][system_name]["interact_with"]
        particle_display_width = settings[_PARTICLES][system_name]["display_width"]
        particle_display_radius = settings[_PARTICLES][system_name]["display_radius"]
        _system = ParticleSystem(system_name, interact_with, is_respawn, display_type, particle_display_width,
                                 particle_display_radius)
        x1 = int(settings[_PARTICLES][system_name]["x1"])
        x2 = int(settings[_PARTICLES][system_name]["x2"])
        y1 = int(settings[_PARTICLES][system_name]["y1"])
        y2 = int(settings[_PARTICLES][system_name]["y2"])
        N = int(settings[_PARTICLES][system_name]["n"])
        color_min = settings[_PARTICLES][system_name]["color"]["min"]
        color_max = settings[_PARTICLES][system_name]["color"]["max"]
        q_min = int(settings[_PARTICLES][system_name]["q_min"])
        q_max = int(settings[_PARTICLES][system_name]["q_max"])
        m_min = int(settings[_PARTICLES][system_name]["m_min"])
        m_max = int(settings[_PARTICLES][system_name]["m_max"])
        v_min = settings[_PARTICLES][system_name]["v_min"]
        v_max = settings[_PARTICLES][system_name]["v_max"]
        _system.set_particle_properties(N, x1, y1, x2, y2, q_min, q_max, m_min, m_max, v_min, v_max, color_min, color_max)
        _system.generate()
    except KeyError as e:
        print(system_name + " is missing options. Skipping this system.")
        print(sys.exc_info()[0])
    particle_systems[system_name] = _system

# is_interact = bool(settings[_PARTICLES]["interact"])

_ORIGIN = "origin"
try: origin_x = int(settings[_ORIGIN]["x"])
except KeyError: origin_x = origin_x_default
try: origin_y = int(settings[_ORIGIN]["y"])
except KeyError: origin_y = origin_y_default
try: is_followmouse = bool(settings[_ORIGIN]["follow_mouse"])
except KeyError: is_followmouse = is_followmouse_default
try: origin_q = int(settings[_ORIGIN]["q"])
except KeyError: origin_q = origin_q_default
try: is_visible = bool(settings[_ORIGIN]["visible"])
except KeyError: is_visible = is_visible_default
try: origin_color = settings[_ORIGIN]["color"] if is_visible else None
except KeyError: origin_color = origin_color_default

origin_particle_system = ParticleSystem(_ORIGIN, [], 0, TYPE_PIXEL)
origin = Point(origin_x, origin_y, origin_q, origin_color)
origin_particle_system.add_particle(origin)
particle_systems[_ORIGIN] = origin_particle_system
