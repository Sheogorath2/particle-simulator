import random

import pygame

from display_types import *


class World:
    def __init__(self, dt, k, x1, y1, x2, y2):
        self.dt = dt
        self.k = k
        self.x1 = min(x1, x2)
        self.y1 = min(y1, y2)
        self.x2 = max(x1, x2)
        self.y2 = max(y1, y2)


class Point:
    def __init__(self, x, y, q, color=None):
        self.x = x
        self.y = y
        self.q = q
        self.color = color

    def draw_pixel(self, screen):
        if self.color:
            screen.set_at((int(self.x), int(self.y)), self.color)

    def draw_circle(self, screen, radius, width):
        if self.color:
            pygame.draw.circle(screen, self.color, (int(self.x), int(self.y)), radius, width)


class Particle(Point):
    def __init__(self, x, y, q, m, vx, vy, color):
        super(Particle, self).__init__(x, y, q, color)
        self.vx = vx
        self.vy = vy
        self.m = m

    def move_point(self, point, world):
        x_new = self.x + world.dt * self.vx
        y_new = self.y + world.dt * self.vy
        dx = self.x - point.x
        dy = self.y - point.y
        coeff = world.dt * (world.k * point.q * self.q) / self.m
        try:
            mul = coeff / (dx * dx + dy * dy) ** 1.5
            vx_new = self.vx + mul * dx
            vy_new = self.vy + mul * dy
            self.vx = vx_new
            self.vy = vy_new
            self.x = x_new
            self.y = y_new
        except ZeroDivisionError:
            print("ZERO DIV!")


class ParticleGenerator:
    def __init__(self, n, x1, y1, x2, y2, q_min, q_max, m_min, m_max, v_min, v_max, color_min, color_max):
        self.x1 = x1
        self.y1 = y1
        self.x2 = x2
        self.y2 = y2
        self.q_min = q_min
        self.q_max = q_max
        self.m_min = m_min
        self.m_max = m_max
        self.v_min = v_min
        self.v_max = v_max
        self.color_min = color_min
        self.color_max = color_max
        self.n = n
        self.current = 0

    def __iter__(self):
        return self

    def __next__(self):
        if self.current < self.n:
            self.current += 1
            return self.get_random_particle()
        self.current = 0
        raise StopIteration()

    def get_random_particle(self):
        return Particle(random.randint(self.x1, self.x2),
                        random.randint(self.y1, self.y2),
                        random.randint(self.q_min, self.q_max),
                        random.randint(self.m_min, self.m_max),
                        random.uniform(self.v_min[0], self.v_max[0]),
                        random.uniform(self.v_min[1], self.v_max[1]),
                        (random.randint(self.color_min[0], self.color_max[0]),
                         random.randint(self.color_min[1], self.color_max[1]),
                         random.randint(self.color_min[2], self.color_max[2])))


class ParticleSystem:
    def __init__(self, name, interact_with, is_respawn, particle_display_type, display_radius=0, display_width=0):
        self.name = name
        self.particle_display_type = particle_display_type
        self.display_radius = display_radius
        self.display_width = display_width
        self.is_respawn = is_respawn
        self.interact_with = interact_with
        self.particle_generator = None
        self.particles = []

    def set_particle_properties(self, n, x1, y1, x2, y2, q_min, q_max, m_min, m_max, v_min, v_max, color_min,
                                color_max):
        self.particle_generator = ParticleGenerator(n, x1, y1, x2, y2, q_min, q_max, m_min, m_max, v_min, v_max,
                                                    color_min,
                                                    color_max)

    def generate(self):
        if self.particle_generator:
            for particle in self.particle_generator:
                self.particles.append(particle)

    def clear(self):
        self.particles = []

    def check_borders(self, x1, y1, x2, y2, regenerate):
        i = 0
        while i < len(self.particles):
            if self.particles[i].x < x1 or self.particles[i].x > x2 \
                    or self.particles[i].y < y1 or self.particles[i].y > y2:
                if regenerate:
                    self.particles[i] = self.particle_generator.get_random_particle()
                else:
                    del self.particles[i]
            i += 1

    def add_particle(self, particle):
        self.particles.append(particle)

    def move(self, world, system_list):
        for particle in self.particles:
            for other_system_name in self.interact_with:
                for other_particle in system_list[other_system_name].particles:
                    if other_particle != particle:
                        particle.move_point(other_particle, world)
        if self.is_respawn: self.check_borders(world.x1, world.y1, world.x2, world.y2, True)

    def draw(self, screen):
        for particle in self.particles:
            if self.particle_display_type == TYPE_PIXEL:
                particle.draw_pixel(screen)
            elif self.particle_display_type == TYPE_CIRCLE:
                particle.draw_circle(screen, self.display_radius, self.display_width)
