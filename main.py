""" PARTICLE SIMULATOR DEMO """

from loadsettings import *
from particles import *

pygame.init()

print(settings)

screen = pygame.display.set_mode((W, H), 0, 32)

if is_fullscreen:
    pygame.display.toggle_fullscreen()

world = World(dt, k, 0, 0, W, H)

t = 0
is_pause = False

while True:
    if not is_pause:
        if is_clearscreen:
            screen.fill(bg_color)

        for system in particle_systems.values():
            system.move(world, particle_systems)
            system.draw(screen)

    origin.draw_pixel(screen)

    t += 1

    pygame.display.update()

    if image_path != 0:
        pygame.image.save(screen, image_path + "hd1_{0:04}".format(t) + ".jpeg")

    if is_followmouse:
        origin.x, origin.y = pygame.mouse.get_pos()

    for event in pygame.event.get():
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE:
                sys.exit()
            if event.key == pygame.K_r:
                for system in particle_systems.values():
                    system.generate()
                    system.draw(screen)
            if event.key == pygame.K_c:
                screen.fill(bg_color)
            if event.key == pygame.K_d:
                for system in particle_systems.values():
                    system.clear()
                origin_particle_system.add_particle(origin)
            if event.key == pygame.K_p:
                is_pause = not is_pause
            if event.key == pygame.K_f:
                pygame.display.toggle_fullscreen()
            if event.key == pygame.K_w:
                for system in particle_systems.values():
                    system.check_borders(0, 0, W, H, False)
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()
